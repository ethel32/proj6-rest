import flask
from flask import request

###
# Globals
###
app = flask.Flask(__name__)

###
# Pages
###

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('index.html')

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)