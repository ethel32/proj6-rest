$(document).ready(function() {
    $("#topCheck").prop("disabled", true)
    $("#topNumber").prop("disabled", true)
    $("#top").prop("disabled", true)
    $("#topCheck").change(function() {
        if ($(this).is(":checked")) {
            $("#topNumber").prop("disabled", false)
        }
    })

    $("#queryType").change(function() {
        if ($("#queryType option:selected").attr("value") !== "listAll") {
            $("#topCheck").prop("disabled", false)
            $("#topNumber").prop("disabled", false)
            $("#top").prop("disabled", false)
        } else {
            $("#topCheck").prop("disabled", true)
            $("#topNumber").prop("disabled", true)
            $("#top").prop("disabled", true)
        }
    })

    $("#query").on("click", function() {
        $("#tables").html("")
        var queryType = $("#queryType option:selected").attr("value")
        var getTopResults = $("#topCheck").is(":checked")
        var numTopResults = $("#topNumber").val()

        var url = "http://0.0.0.0:5000/" + queryType
        if (getTopResults) {
            url += "?top=" + numTopResults
        }

        $.get(url, function(data) {
            // $("#space").text(JSON.stringify(data, null, "\t"))
            for(var i = 0; i < data["brevets"].length; i++) {
                var tableHTML = "<table>"
                var brevet = data["brevets"][i];
                tableHTML += "<tr><th>Brevet Number</th><th>Distance</th><th>Begin Time</th><th>Begin Date</th></tr>"
                tableHTML += "<tr><td>" + (i+1) + "</td><td>" + brevet["distance"] + "</td><td>" + brevet["begin_time"] + "</td><td>" + brevet["begin_date"] + "</td></tr>"
                tableHTML += "<tr><th>Miles</th><th>Kilometers</th><th>Location</th>"
                for (var c = 0; c < brevet["controls"].length; c++) {
                    var control = brevet["controls"][c]
                    
                    if ("open" in control) {
                        tableHTML += "<th>Open</th>"
                    }
                    if ("close" in control) {
                        tableHTML += "<th>Close</th>"
                    }
                    tableHTML += "</tr>"

                    tableHTML += "<tr><td>" + control["mi"] + "</td><td>" + control["km"] + "</td>"
                    
                    if (control["location"] != null) {
                        tableHTML += "<td>" + control["location"] + "</td>"
                    } else {
                        tableHTML += "<td>None</td>"
                    }
                    
                    if ("open" in control) {
                        tableHTML += "<td>" + control["open"] + "</td>"
                    }
                    if ("close" in control) {
                        tableHTML += "<td>" + control["close"] + "</td>"
                    }
                    
                    tableHTML += "</tr>"
                }
                tableHTML += "</table>"
                $("#tables").append(tableHTML)
            }
            

            console.log(data)
        })
    })
});