from logging import NullHandler
import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import acp_times
import arrow
import csv
import io
import operator

app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return render_template('calc.html')

def getMasterDict():
    _items = db.tododb.find()

    # items = [item for item in _items]
    masterDict = {
        "brevets": []
    }
    for item in _items:
        brevetDict = {
            "distance": int(item["distance"]),
            "begin_date": item["begin_date"],
            "begin_time": item["begin_time"],
            "controls": []
        }
        for control in item["controls"]:
            controlDict = {
                'open': control["open"],
                'close': control["close"],
                'mi': float(control["distM"]),
                'km': float(control["distKM"])
            }
            try:
                controlDict["location"] = control["location"]
            except KeyError:
                controlDict["location"] = None
            
            brevetDict["controls"].append(controlDict)
        masterDict["brevets"].append(brevetDict)
    
    return masterDict

def convertMasterDictToCSV(masterDict, open=True, close=True):
    fields = ["brevets/distance", "brevets/begin_date", "brevets/begin_time"]
    controlsFielded = -1
    rows = []
    for brevetDict in masterDict["brevets"]:
        row = []
        row.append(brevetDict["distance"])
        row.append(brevetDict["begin_date"])
        row.append(brevetDict["begin_time"])

        c = 0
        controlFields = ["km", "mi", "location"]

        if open:
            controlFields.append("open")
        if close:
            controlFields.append("close")
        
        for control in brevetDict["controls"]:
            if controlsFielded < c:
                for field in controlFields:
                    fields.append("brevets/controls/{}/{}".format(c, field))
                controlsFielded += 1
            
            for f in controlFields:
                row.append(control[f])
            
            c += 1
        
        rows.append(row)
    
    csvString = ""
    i = 0
    for item in fields:
        if i != len(fields) - 1:
            csvString += str(item) + ","
        else:
            csvString += str(item)
        i += 1

    csvString += "\n"
    
    for row in rows:
        i = 0
        for item in row:
            if i != len(row) - 1:
                csvString += str(item) + ","
            else:
                csvString += str(item)
            i += 1
        csvString += "\n"
    
    return csvString

@app.route("/listAll", methods=['GET'])
@app.route("/listAll/json", methods=['GET'])
def listAll():
    return flask.jsonify(getMasterDict())

@app.route("/listAll/csv", methods=['GET'])
def listAllCSV(open=True, close=True):
    masterDict = getMasterDict()
    return convertMasterDictToCSV(masterDict), {"Content-Type": "text/plain"}

def getOpenCloseDict(open=True, top=None):
    if open:
        keep = "open"
        remove = "close"
    else:
        keep = "close"
        remove = "open"

    masterDict = getMasterDict()
    if top is not None:
        topDict = {
            "brevets": []
        }
        controlOpens = []
        for brevet in masterDict["brevets"]:
            for control in brevet["controls"]:
                opensDict = {
                    "distance": brevet["distance"],
                    "begin_date": brevet["begin_date"],
                    "begin_time": brevet["begin_time"],
                    "controls": [{
                        "km": control["km"],
                        "mi": control["mi"],
                        "location": control["location"],
                        keep: control[keep]
                    }],
                    keep: control[keep]
                }
                controlOpens.append(opensDict)
        
        controlOpens.sort(key = operator.itemgetter(keep))
        for opens in controlOpens:
            opens.pop(keep)
        
        last = len(controlOpens) - 1
        topDict["brevets"] = controlOpens[last-top:last]
        
        return topDict
    else:
        for brevet in masterDict["brevets"]:
            for control in brevet["controls"]:
                control.pop(remove)

        return masterDict

@app.route('/listOpenOnly', methods=['GET'])
@app.route('/listOpenOnly/json', methods=['GET'])
def listOpen():
    top = request.args.get("top", type=int)
    return flask.jsonify(getOpenCloseDict(True, top))
            
@app.route('/listOpenOnly/csv', methods=['GET'])
def listOpenCSV():
    top = request.args.get("top", type=int)
    return convertMasterDictToCSV(getOpenCloseDict(True, top), True, False), {"Content-Type": "text-plain"}

@app.route('/listCloseOnly', methods=['GET'])
@app.route('/listCloseOnly/json', methods=['GET'])
def listClose():
    top = request.args.get("top", type=int)
    return flask.jsonify(getOpenCloseDict(False, top))
            
@app.route('/listCloseOnly/csv', methods=['GET'])
def listCloseCSV():
    top = request.args.get("top", type=int)
    return convertMasterDictToCSV(getOpenCloseDict(False, top), False, True), {"Content-Type": "text-plain"}

@app.route('/display', methods=['GET', 'POST'])
def display():
    _items = db.tododb.find()
    items = []
    for item in _items:
        for doc in item['controls']:
            items.append(doc)
    
    if len(items) > 0:
        return render_template('display.html', items=items)
    else:
        return render_template("error.html")
    

@app.route('/submit', methods=['POST'])
def submit():
    openTimes = request.form.getlist("open")
    closeTimes = request.form.getlist("close")
    locations = request.form.getlist("location")
    distKMs = request.form.getlist("km")
    distMs = request.form.getlist("miles")
    noteses = request.form.getlist("notes")
    distance = request.form.getlist("distance")[0]
    begin_date = request.form.getlist("begin_date")[0]
    begin_time = request.form.getlist("begin_time")[0]

    count = 0
    item_docs = []
    # parameters = ["open", "close", "location", "km", "miles", "notes"]
    for i in range(len(openTimes)):
        if openTimes[i] != "":
            count += 1
            item_doc = {
                'open': openTimes[i],
                'close': closeTimes[i] if len(closeTimes) > i else "",
                'location': locations[i] if len(locations) > i else "",
                'distKM': distKMs[i] if len(distKMs) > i else "",
                'distM': distMs[i] if len(distMs) > i else "",
                'notes': noteses[i] if len(noteses) > i else ""
            }
            item_docs.append(item_doc)
    
    if count == 0:
        return render_template("error.html")
    else:
        brevetDict = {
            'distance': distance,
            'begin_date': begin_date,
            'begin_time': begin_time,
            'controls': item_docs
        }
        db.tododb.insert_one(brevetDict)
    
    return render_template("calc.html")

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    notes = ""

    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    
    brevet_dist_km = request.args.get("brevetDistKm", 200, type=int)
    begin_time = request.args.get("beginTime", "", type=str)
    date = request.args.get("beginDate", "", type=str)
    
    # formatting to be passed into open & close time
    start_iso = arrow.get("{} {}".format(date, begin_time)).isoformat()
    
    # get open and close times
    open_time = acp_times.open_time(km, brevet_dist_km, start_iso)
    close_time = acp_times.close_time(km, brevet_dist_km, start_iso)

    # error handling (warnings taken from the ACP calculator)
    percentWarn = (km > 0 and (((km - brevet_dist_km) / km) > 0.2))
    zeroWarn = (km == 0)

    if zeroWarn:
        notes = "The distance is zero"
    
    if percentWarn:
        notes = "The distance is 20 percent or longer than a standard brevet"

    return flask.jsonify(result={"open": open_time, "close": close_time, "notes": notes})

@app.after_request
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = 'http://localhost:5001'
    return response

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
