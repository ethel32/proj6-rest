# ACP Controle Times calculator with REST
by Ethel Arterberry (earterb3@uoregon.edu) with database functionality

Reimplemented the RUSA ACP controle time calculator with flask, ajax, and mongodb.

There is also now a RESTful layer that reads items from the database and provides them in either CSV or JSON. It has the following resources:

* `/listAll`: Lists all brevets in the database
* `/listAll/csv`: Lists all brevets in the database in CSV format
* `/listOpenOnly`: Lists only open times from the database
* `/listOpenOnly/csv`: Lists only open times from the database in CSV format
* `/listOpenOnly/csv?top=n`: Lists top n open times from the database in CSV format
* `/listOpenOnly?top=n`: Lists top n open times from the database in JSON format
* `/listCloseOnly`: Lists only close times from the database
* `/listCloseOnly/csv`: Lists only close times from the database in CSV format
* `/listCloseOnly/csv?top=n`: Lists top n close times from the database in CSV format
* `/listCloseOnly?top=n`: Lists top n close times from the database in JSON format

Credits to Michal Young for the initial version of this code.

# Purpose
This calculates the open and close times for standard ACP brevets with lengths of 200km, 300km, 400km, 600km and 1000km. It allows you to input checkpoints into a web inteface.

- Rounds seconds in a standard fashion.
- Error messages from the original calculator are kept and passed into the notes. If the control is between 0 and 20% longer than a brevet, it will be rounded to the nearest brevet. If it's 20% or longer, a message will be thrown

Controle times are calculated according to this table

|Location(km)|Min speed (km/hr)|Max speed(km/hr)|
|------------|---------|---------|
|0 - 200     |15    |34|
|200 - 400   |15    |32|
|400 - 600   |15    |30|
|600 - 1000  |11.428|28|
|1000 - 1300 |13.333|26|

There are also manual test cases for the database features included in the `tests.txt` file within DockerMongo folder.

## Rest Client App
Included is also a simple client app using a Flask server and JQuery/HTML/JS/CSS that queries the REST server and displays its information in human-readable tables.